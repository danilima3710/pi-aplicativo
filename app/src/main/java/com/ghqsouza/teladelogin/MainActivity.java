package com.ghqsouza.teladelogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText editLogin;
    EditText editSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editLogin = findViewById(R.id.logintxt);
        editSenha = findViewById(R.id.senhatxt);
    }

    public void logar(View view) {
        Intent i = new Intent(this, TelaPrincipal.class);
        i.putExtra("login", "ed");

            startActivity(i);

        }
    }

